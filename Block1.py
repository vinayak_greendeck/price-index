# import json
# import os
# from datetime import datetime

# import pandas as pd
# from elasticsearch import Elasticsearch
# from pandas import ExcelWriter

# es_url = os.getenv('ELASTICSEARCH_URL')

# def get_es_conn(es_url):
#     return Elasticsearch(es_url, port=80, timeout=60)

# def get_sale_ids(es):
#     print("___________________ INSIDE GET SALE IDS _____________________________")
#     query = {
#         "_source": ["sale_id","website_id"],
#         "track_total_hits":True,
#         "query":{
#             "bool":{
#             "must":[
#                 {
#                 "range": {
#                     "created_at": {
#                     "gte": "2021-09-10",
#                     "lte": "2021-12-09"
#                     }
#                 }
#                 },
#                 {
#                 "match":{
#                     "website_id.keyword":"5db9d70d6d97010001f81d78"
#                 }}
#             ]}
#             }
#         }
#     try:
#         data  = es.search(index = "sales", body=query, scroll='2m', size=5)
#         sid = data['_scroll_id']
#         scroll_size = len(data['hits']['hits'])
#         print('scroll id', sid)
#         while scroll_size > 0:
#             print("Got %d Hits at %s" % (scroll_size, str(datetime.now())))
#             # Before scroll, process current batch of hits
#             parsing(data['hits']['hits'])
#             data = es.scroll(scroll_id=sid, scroll='2m')
#             # Update the scroll ID
#             sid = data['_scroll_id']
#             # Get the number of results that returned in the last scroll
#             scroll_size = len(data['hits']['hits'])
#     except:
#         print("ERROR IN GET SALE ID")
#     return data

# def get_prod_refs(es,sale_id_list):
#     print("___________________ INSIDE GET PROD REFS _____________________________")
#     srp_sale_id_dict = {}
#     try:
#         for sale_id in sale_id_list:
#             print("FOUND ---> {}".format(sale_id["_source"]["sale_id"]))
#             ref_list,result = get_all_refs(es,sale_id["_source"]["sale_id"])
#             if result:
#                 srp_sale_id_dict[sale_id["_source"]["sale_id"]] = ref_list
#             else:
#                 continue
#         return srp_sale_id_dict
#     except:
#         print("ERROR IN GET SALE ID")

# def get_all_refs(es,sale_id):
#     print("___________________ INSIDE GET ALL REF _____________________________")
#     ref_list = []
#     query = {
#     "_source": ["meta.reference","meta.sale_id","name","price.offer_price.value"],
#     "track_total_hits":True,
#     "query":{
#         "bool":{
#         "must":[
#             {
#             "range": {
#                 "created_at": {
#                 "gte": "2021-09-10",
#                 "lte": "2021-12-09"
#                 }
#             }
#             },
#             {
#             "match":{
#                 "meta.sale_id.keyword":"{}".format(sale_id)
#             }}
#         ]}
#         }
#     }
#     data  = es.search(index = "products_5db9d70d6d97010001f81d78", body=query, scroll='2m', size=10000)
#     if len(data["hits"]["hits"]) > 0:
#         for product in data["hits"]["hits"]:
#             ref_list.append(product["_source"]["meta"]["reference"])
#         return ref_list,True
#     else:
#         return [],False

# def get_veepee_sale(es,srp_sale_id_dict):
#     srp_veepee_dict = {}
#     veepee_ref_list = []
#     try:
#         for sale_id in srp_sale_id_dict.keys():
#             print("MATCHING -------------> {}".format(sale_id))
#             for ref in srp_sale_id_dict[sale_id]:
#                 ref_dict = {}
#                 sale_ref_list,result = get_product_matching(es,ref)
#                 print("RESULT --------> ",result)
#                 if result:
#                     ref_dict[ref] = sale_ref_list
#                     veepee_ref_list.append(ref_dict)
#                 else:
#                     continue
#             srp_veepee_dict[sale_id] = veepee_ref_list
#         print(srp_veepee_dict)
#         return srp_veepee_dict
#     except Exception as e:
#         print("ERROR IN GET VEEPEE SALES",e)

# def get_product_matching(es,ref):
#     try:
#         print("PRODUCT REF -------> {}".format(ref))
#         query = {
#             "_source": ["meta.reference","website_id","meta.sale_id","name","price.offer_price.value","created_at"],
#             "query": {
#                 "bool": {
#                     "should": get_should_query_list(ref),
#                     "minimum_should_match": 1,
#                     "must": [
#                         {"range": {"created_at": {
#                             "gte": "2021-09-10",
#                             "lte": "2021-12-09"
#                             }
#                         }},
#                         {"terms": {"website_id": ["5d20aa3b147b120001686f66"]}}
#                     ]
#                 }
#             },
#             "sort":
#                 {"created_at": "DESC"},
#             "from": 0,
#             "size": 20
#             }
#         data  = es.search(index = "products_5d20aa3b147b120001686f66", body=query, scroll='2m', size=10000)
#         veepee_ref_list = []
#         if len(data["hits"]["hits"]) > 0:
#             for prod in data["hits"]["hits"]:
#                 offer_price = prod["_source"]["price"]["offer_price"]["value"]
#                 reference = prod["_source"]["meta"]["reference"]
#                 sale_id = prod["_source"]["meta"]["sale_id"]
#                 name = prod["_source"]["name"]
#                 veepee_ref_list.append({"name":name,"reference":reference,"sale_id":sale_id,"offer_price":offer_price})
#             return veepee_ref_list,True
#         else:
#             return [],False
#     except Exception as e:
#         print("ERROR IN GET PRODUCT MATCHING",e)

# def get_should_query_list(supplier_ref):
#     try:
#         should_query_list = []
#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace(" - ","-").strip()}})
#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('.','-').strip()}})
#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('.','_').strip()}})

#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('_','-').strip()}})
#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('-','_').strip()}})

#         should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).strip()}})
#         should_query_list.append({"term": {"meta.reference.keyword": str(supplier_ref).strip().lower()}})

#         should_query_list.append({"term": {"meta.reference.keyword": get_modify_supplier_ref(supplier_ref)}})
#         should_query_list.append({"term": {"meta.reference.keyword":get_modify_supplier_ref(supplier_ref).strip().lower()}})
#         return should_query_list
#     except:
#         print("ERROR IN GET SHOULD QUERY LIST")

# def get_modify_supplier_ref(supplier_ref):

#     """
#     Modifies supplier reference by removing spaces from the end and replacing in between space with -

#     Args:
#         supplier_ref(str): Supplier Reference which needs to be modified

#     Returns:
#         modified_supplier_ref(str): Modified supplier reference
#     """
#     try:
#         supplier_ref_text_list = str(supplier_ref).strip().split(" ")

#         if len(supplier_ref_text_list)>0:
#             modified_supplier_ref = "-".join(supplier_ref_text_list).strip()
#         else:
#             modified_supplier_ref = supplier_ref.strip()
#         return modified_supplier_ref
#     except:
#         print("ERROR IN GET MODIFY SUPP REF")

# if __name__ == "__main__":
#     df_lists = []
#     try:
#         es = get_es_conn(es_url)
#         print("CONNECTION ESTABLISHED")
#     except:
#         print("ERROR IN CONNECTION")
#         raise
#     try:
#         data = get_sale_ids(es)
#         print("DATA COLLECTED")
#         print("SALE IDs FOUND --------> ",len(data["hits"]["hits"]))
#     except:
#         print("ERROR IN DATA COLLECTION")
#     try:
#         srp_sale_id_dict = get_prod_refs(es,data["hits"]["hits"])
#         print("SRP DICTIONARY CREATED")
#     except:
#         print("ERROR IN REF COLLECTION")
#     print("______________________________________________ SRP _____________________________________")
#     print(srp_sale_id_dict)
#     print("_____________________________________________ VEEPEE _____________________________________")
#     try:
#         result = get_veepee_sale(es,srp_sale_id_dict)
#         print(result)
#     except:
#         print("ERROR IN GET VEEPEE SALE ID DICTIONARY")