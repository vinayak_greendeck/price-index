from elasticsearch import Elasticsearch
from datetime import datetime
import os
import pandas as pd
import json
from pandas import ExcelWriter

es_url = os.getenv('ELASTICSEARCH_URL')
level = []

#Connection to es
def get_es_conn(es_url):
    return Elasticsearch(es_url, port=80, timeout=60)

#Calculating price index on product level
def parsing(products):
    for data in products:
        #data = json.loads(data)
    
        try:
            veepee_price = data['_source']['similar_products']['website_results']['5d20aa3b147b120001686f66']['meta']['avg_price']['offer']
            
            try:
                srp_price = data['_source']['price']['offer_price']['value']
            except:
                raise    
             
        
            price_index = 100 + (((srp_price/veepee_price) - 1) * 100)
            
            level.append({'Flash Sale ID' : data['_source']['meta']['sale_id'],
                        'Category' : data['_source']['classification']['l1'],
                        'Brand' : data['_source']['brand']['name'],
                        'Supplider Reference' : data['_source']['meta']['reference'],
                        'SRP offer price' : srp_price, 'Veepee offer price' : veepee_price, 'Price Index' : price_index,
                        'Product frequency in veepee fr' : len(data['_source']['similar_products']['website_results']['5d20aa3b147b120001686f66']['knn_items'])
                        })
            
        except Exception as e:
            print('No price found in veepee', e)
            raise

    #print('Price index for sku',data['_source']['sku'], 'is' ,price_index)

#Saving excel file
def save_xls(list_dfs):
    with ExcelWriter('Price Index_frequency.xlsx') as writer:
        for df, sheet_name in zip(list_dfs,['Product Level', 'Sale Level']):
            df.to_excel(writer,sheet_name, index=False)
        writer.save()

#Get Matched percentage
def get_matching(sale_id, matched_count):
    
    try:
        query = {
                "query": {
                    "match": {
                        "meta.sale_id": str(sale_id)
                        }
                    }
                }

        sale_count  = es.count(index = "products_5db9d70d6d97010001f81d78", body=query)

        return round((matched_count/sale_count['count'])*100, 2)

    except Exception as e:
        print('Error in getting sale count',e, sale_id)
        raise

#Calculating price index on brand, category and sale level
def calculate_index(data, group_by):
    new_data = []
    for i in data:
        if group_by == 'meta.sale_id.keyword':
            if i['doc_count'] > 40:
                price_index = 100 + (((i['Average_offer_price_srp']['value']/i['Average_offer_price_veepee']['value']) - 1) * 100)

                i['Price Index'] = price_index
                i['Average_offer_price_srp'] = i['Average_offer_price_srp']['value']
                i['Average_offer_price_veepee'] = i['Average_offer_price_veepee']['value']
                i['Matching %'] = get_matching(i['key'], i['doc_count'])
                
                print('Price index for', i['key'], 'is', price_index)
                new_data.append(i)

        else:
            price_index = 100 + (((i['Average_offer_price_srp']['value']/i['Average_offer_price_veepee']['value']) - 1) * 100)

            i['Price Index'] = price_index
            i['Average_offer_price_srp'] = i['Average_offer_price_srp']['value']
            i['Average_offer_price_veepee'] = i['Average_offer_price_veepee']['value']
            print('Matched Percentage',get_matching(i['key'], i['doc_count']))
           

            print('Price index for', i['key'], 'is', price_index)
            new_data.append(i)

    return new_data


# ES query + ES connection
if __name__ == "__main__":
    df_lists = []
    es = get_es_conn(es_url)
    #Finding price index on product level
    query = {
            "track_total_hits":True,
            "query":{
                "bool":{
                "must":[
                    {
                    "range": {
                        "created_at": {
                        "gte": "2021-10-25",
                        "lte": "2021-11-02"
                        }
                    }
                    },
                    {"bool":
                    {"should":[
                        {"exists":
                        {"field":"similar_products.website_results.5d20aa3b147b120001686f66.knn_items"}
                        }
                    ]}
                    }
                ]}
                },
                "_source":{"excludes":["meta.base64"], "includes": ["classification.l1", "brand.name", "price.offer_price.value", "similar_products.website_results.5d20aa3b147b120001686f66.meta.avg_price.offer", "meta.reference", "meta.sale_id", "similar_products.website_results.5d20aa3b147b120001686f66.knn_items"]},
                "sort":[
                {"similar_products.meta.total_results":{"order":"desc"}},
                {"updated_at":{"order":"desc"}}
            ]
            }

    data  = es.search(index = "products_5db9d70d6d97010001f81d78", body=query, scroll='2m', size=10000)

    sid = data['_scroll_id']
    scroll_size = len(data['hits']['hits'])
    print('scroll id', sid)
    while scroll_size > 0:
        print("Got %d Hits at %s" % (scroll_size, str(datetime.now())))
        # Before scroll, process current batch of hits
        parsing(data['hits']['hits'])
        data = es.scroll(scroll_id=sid, scroll='2m')
        # Update the scroll ID
        sid = data['_scroll_id']
        # Get the number of results that returned in the last scroll
        scroll_size = len(data['hits']['hits'])

    table = pd.DataFrame(level)
    df_lists.append(table)
    
    #Finding price index using aggregations
    for group_by in ['meta.sale_id.keyword']:
        query = {
        "size" : 0,
        "track_total_hits" : True,
        "query":{
            "bool":{
            "must":[
                {
                "range": {
                    "created_at": {
                    "gte": "2021-11-02",
                    "lte": "2021-11-02"
                    }
                }
                },
                {"bool":
                {"should":[
                    {"exists":
                    {"field":"similar_products.website_results.5d20aa3b147b120001686f66.knn_items"}
                    }
                ]}
                }
            ]}
            },
            "aggs": {
            "Data": {
                "terms": {
                "field": group_by,
                "size": 10000
                },
                "aggs": {
                "Average_offer_price_srp": {
                    "avg": {
                    "field": "price.offer_price.value"
                    }
                },
                "Average_offer_price_veepee": {
                    "avg": {
                    "field": "similar_products.website_results.5d20aa3b147b120001686f66.meta.avg_price.offer"
                    }
                }
                }
            }
            }, 
            "_source":{"excludes":["meta.base64"]},
            "sort":[
            {"similar_products.meta.total_results":{"order":"desc"}},
            {"updated_at":{"order":"desc"}}
            ]}

        
        print('Extracting.....')
        data  = es.search(index = "products_5db9d70d6d97010001f81d78", body=query)

        print('Total number of docs',len(data['aggregations']['Data']['buckets']))

        new_data = calculate_index(data['aggregations']['Data']['buckets'], group_by)
        print('Done for',group_by)

        table = pd.DataFrame(new_data)
        print(table.head())

        df_lists.append(table)

    save_xls(df_lists)