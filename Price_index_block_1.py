import json
import os
from datetime import datetime

import pandas as pd
from elasticsearch import Elasticsearch
from pandas import ExcelWriter

SALE_ID_LIST = []

es_url = os.getenv('ELASTICSEARCH_URL')

def get_es_conn(es_url):
    return Elasticsearch(es_url, port=80, timeout=60)

def getting_refs(data_list):
    REF_ID_LIST = []
    for data in data_list:
        REF_ID_LIST.append(data["_source"]["meta"]["reference"])
    return REF_ID_LIST

def getting_sales(data_list):
    global SALE_ID_LIST
    for data in data_list:
        SALE_ID_LIST.append(data["_source"]["sale_id"])

def getting_matches(data_list):
    try:
        REF_MATCH_LIST = []
        for data in data_list:
            ref_data = {}
            ref_data["sale_id_vp"] = data["_source"]["meta"]["sale_id"]
            ref_data["offer_price_vp"] = data["_source"]["price"]["offer_price"]["value"]
            ref_data["name_vp"] = data["_source"]["name"]
            ref_data["ref_vp"] = data["_source"]["meta"]["reference"]
            REF_MATCH_LIST.append(ref_data)
        return REF_MATCH_LIST
    except Exception as e:
        print("EEEEEE",e)

def get_sale_ids(es):
    print("___________________ INSIDE GET SALE IDS _____________________________")
    query = {
        "_source": ["sale_id","website_id"],
        "track_total_hits":True,
        "query":{
            "bool":{
            "must":[
                {
                "range": {
                    "created_at": {
                    "gte": "2021-12-02",
                    "lte": "2021-12-09"
                    }
                }
                },
                {
                "match":{
                    "website_id.keyword":"5db9d70d6d97010001f81d78"
                }}
            ]}
            }
        }
    try:
        ref_list = []
        data  = es.search(index = "sales", body=query, scroll='2m', size=100)
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])
        #print('scroll id', sid)
        while scroll_size > 0:
            #print("Got %d Hits at %s" % (scroll_size, str(datetime.now())))
            # Before scroll, process current batch of hits
            getting_sales(data['hits']['hits'])
            data = es.scroll(scroll_id=sid, scroll='2m')
            # Update the scroll ID
            sid = data['_scroll_id']
            # Get the number of results that returned in the last scroll
            scroll_size = len(data['hits']['hits'])
        return ref_list
    except:
        print("ERROR IN GET SALE ID")
    return data

def get_all_refs(es,sale_id):
    print("___________________ INSIDE GET ALL REF _____________________________")
    sale_id = "91280"
    print("IN LOOP FOR ----> ",sale_id)
    ref_list = []
    try:
        query = {
        "_source": ["meta.reference","meta.sale_id","name","price.offer_price.value"],
        "track_total_hits":True,
        "query":{
            "bool":{
            "must":[
                {
                "range": {
                    "created_at": {
                    "gte": "2021-12-02",
                    "lte": "2021-12-09"
                    }
                }
                },
                {
                "match":{
                    "meta.sale_id.keyword":"{}".format(sale_id)
                }}
            ]}
            }
        }
        data  = es.search(index = "products_5db9d70d6d97010001f81d78", body=query, scroll='2m', size=100)
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])
        #print('scroll id', sid)
        while scroll_size > 0:
            #print("Got %d Hits at %s" % (scroll_size, str(datetime.now())))
            # Before scroll, process current batch of hits
            for ref in getting_refs(data['hits']['hits']):
                    ref_list.append(ref)
            data = es.scroll(scroll_id=sid, scroll='2m')
            # Update the scroll ID
            sid = data['_scroll_id']
            # Get the number of results that returned in the last scroll
            scroll_size = len(data['hits']['hits'])
        return sale_id,ref_list
    except Exception as e:
        print(e)

def get_product_matching(es,sale_id,srp_sale_id_dict):
    print("___________________ INSIDE GET MATCHING _____________________________")
    ref_list_srp = srp_sale_id_dict[sale_id]
    try:
        ref_list_dict = {}
        for ref in ref_list_srp:
            ref_list = []
            print("PRODUCT REF -------> {}".format(ref))
            query = {
                "_source": ["meta.reference","website_id","meta.sale_id","name","price.offer_price.value","created_at"],
                "query": {
                    "bool": {
                        "should": get_should_query_list(ref),
                        "minimum_should_match": 1,
                        "must": [
                            {"range": {"created_at": {
                                "gte": "2021-09-10",
                                "lte": "2021-12-09"
                                }
                            }},
                            {"terms": {"website_id": ["5d20aa3b147b120001686f66"]}}
                        ]
                    }
                },
                "sort":
                    {"created_at": "DESC"},
                "from": 0,
                "size": 20
                }
            data  = es.search(index = "products_5d20aa3b147b120001686f66", body=query, scroll='2m', size=100)
            sid = data['_scroll_id']
            scroll_size = len(data['hits']['hits'])
            #print('scroll id', sid)
            while scroll_size > 0:
                #print("Got %d Hits at %s" % (scroll_size, str(datetime.now())))
                # Before scroll, process current batch of hits
                try:
                    for ref_data in getting_matches(data['hits']['hits']):
                            ref_list.append(ref_data)
                except Exception as e:
                    print("ERROR IN GETTING MATCH WALA DICT",e)
                data = es.scroll(scroll_id=sid, scroll='2m')
                # Update the scroll ID
                try:
                    sid = data['_scroll_id']
                except:
                    print("NOT GOT")
                # Get the number of results that returned in the last scroll
                try:
                    scroll_size = len(data['hits']['hits'])
                    if scroll_size > 0:
                        continue
                    else:
                        break
                except Exception as e:
                    print("------------------>",e)
            try:
                #print(ref,end="------->")
                #print(ref_list)
                ref_list_dict[ref] = ref_list
            except Exception as e:
                print("____----________----->",e)
        return sale_id,ref_list_dict
    except Exception as e:
        print("BADA WALA ERROR 3",e)

def get_should_query_list(supplier_ref):
    try:
        should_query_list = []
        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace(" - ","-").strip()}})
        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('.','-').strip()}})
        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('.','_').strip()}})

        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('_','-').strip()}})
        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).replace('-','_').strip()}})

        should_query_list.append({ "term" : { "meta.reference.keyword" : str(supplier_ref).strip()}})
        should_query_list.append({"term": {"meta.reference.keyword": str(supplier_ref).strip().lower()}})

        should_query_list.append({"term": {"meta.reference.keyword": get_modify_supplier_ref(supplier_ref)}})
        should_query_list.append({"term": {"meta.reference.keyword":get_modify_supplier_ref(supplier_ref).strip().lower()}})
        return should_query_list
    except:
        print("ERROR IN GET SHOULD QUERY LIST")

def get_modify_supplier_ref(supplier_ref):

    """
    Modifies supplier reference by removing spaces from the end and replacing in between space with -

    Args:
        supplier_ref(str): Supplier Reference which needs to be modified

    Returns:
        modified_supplier_ref(str): Modified supplier reference
    """
    try:
        supplier_ref_text_list = str(supplier_ref).strip().split(" ")

        if len(supplier_ref_text_list)>0:
            modified_supplier_ref = "-".join(supplier_ref_text_list).strip()
        else:
            modified_supplier_ref = supplier_ref.strip()
        return modified_supplier_ref
    except:
        print("ERROR IN GET MODIFY SUPP REF")

def runner():
    global SALE_ID_LIST
    df_lists = []
    try:
        es = get_es_conn(es_url)
        print("CONNECTION ESTABLISHED")
    except:
        print("ERROR IN CONNECTION")
        raise
    try:
        get_sale_ids(es)
        print("DATA COLLECTED")
        print(len(SALE_ID_LIST),"------->",SALE_ID_LIST)
    except:
        print("ERROR BADA WALA 1")
    try:
        srp_sale_id_dict = {}
        for sale_id in SALE_ID_LIST:
            s_id,ref_list = get_all_refs(es,sale_id)
            srp_sale_id_dict[s_id] = ref_list
            break
        print(srp_sale_id_dict)
    except Exception as e:
        print("ERROR BADA WALA 2",e)
    try:
        srp_vp_dict = {}
        for sale_id in srp_sale_id_dict.keys():
            sale_id_result,ref_list = get_product_matching(es,sale_id,srp_sale_id_dict)
            srp_vp_dict[sale_id_result] = ref_list
        print(srp_vp_dict)
    except Exception as e:
        print("ERROR IN GET PM",e)

if __name__ == "__main__":
    runner()